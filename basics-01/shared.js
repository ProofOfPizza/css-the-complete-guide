var backdrop = document.querySelector(".backdrop");
var selectPlanButtons = document.querySelectorAll(".plan button");
var modal = document.querySelector(".modal");
var toggleButton = document.querySelector(".toggle-button");
var mobileNav = document.querySelector(".mobile-nav");
var buttonModalNo = document.querySelector(".modal__action--negative");
var ctaButton = document.querySelector(".main-nav__item--cta");

for (var button of selectPlanButtons) {
  button.addEventListener("click", () => {
    // modal.style.display = "block";
    backdrop.style.display = "block";
    setTimeout(() => {
      modal.classList.add("open");
      backdrop.classList.add("open");
    }, 50);
  });
}

backdrop.addEventListener("click", () => {
  closeAllModalsAndBackdrop();
});

function closeAllModalsAndBackdrop() {
  // mobileNav.style.display = "none";
  // modal.style.display = "none";
  // backdrop.style.display = "none";
  console.log("closing", mobileNav);
  if (mobileNav) {
    mobileNav.classList.remove("open");
  }
  if (modal) {
    modal.classList.remove("open");
  }
  backdrop.classList.remove("open");
  setTimeout(() => {
    backdrop.style.display = "none";
  }, 200);
}

if (buttonModalNo) {
  buttonModalNo.addEventListener("click", closeAllModalsAndBackdrop);
}

toggleButton.addEventListener("click", () => {
  backdrop.style.display = "block";
  setTimeout(() => {
    backdrop.classList.add("open");
    mobileNav.classList.add("open");
  }, 50);
});

ctaButton.addEventListener("animationstart", (event) => {
  console.log("animation started", event);
});
ctaButton.addEventListener("animationend", (event) => {
  console.log("animation end", event);
});
ctaButton.addEventListener("animationiteration", (event) => {
  console.log("animation iteration", event);
});

